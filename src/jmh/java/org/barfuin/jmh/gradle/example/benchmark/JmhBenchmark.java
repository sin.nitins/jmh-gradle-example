/*
 * Copyright 2021 the jmh-gradle-example contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.jmh.gradle.example.benchmark;

import java.util.concurrent.TimeUnit;

import org.barfuin.jmh.gradle.example.StringOfSpaces;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;


/**
 * These are the performance tests.
 */
@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 3, time = 5, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 3, time = 5, timeUnit = TimeUnit.SECONDS)
public class JmhBenchmark
{
    /**
     * We test string lengths of:<ul>
     * <li>1 - very short</li>
     * <li>15 - short</li>
     * <li>170 - longer, but within the 177 characters of our predefined string SPACES_177</li>
     * <li>178 - one more than SPACES_177 can provide</li>
     * <li>17780 - large, but not a multiple of 177</li>
     * </ul>
     */
    @Param({"1", "15", "170", "178", "17780"})
    public int length;



    @Benchmark
    public String testByStream()
    {
        return StringOfSpaces.byStream(length);
    }



    @Benchmark
    public String testBySubstring()
    {
        return StringOfSpaces.bySubstring(length);
    }



    @Benchmark
    public String testBySubstringOrStream()
    {
        return StringOfSpaces.bySubstringOrStream(length);
    }



    @Benchmark
    public String testByStringBuilder()
    {
        return StringOfSpaces.byStringBuilder(length);
    }



    @Benchmark
    public String testByTrivialConcat()
    {
        return StringOfSpaces.byTrivialConcat(length);
    }



    @Benchmark
    public String testByArrayFill()
    {
        return StringOfSpaces.byArrayFill(length);
    }



    @Benchmark
    public String testByJava11Repeat()
    {
        return StringOfSpaces.byJava11Repeat(length);
    }



    @Benchmark
    public String testByCommonsLang3()
    {
        return StringOfSpaces.byCommonsLang3(length);
    }
}
